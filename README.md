# docker-bind

DockerhubとGithubの連携

## 概要

DockerhubのレポジトリをGithubに連携させてGithubにpushするとDockerhubのimageが生成される

## 作業

確認

    $ docker ps
    CONTAINER ID        IMAGE                                                COMMAND                  CREATED             STATUS              PORTS                        NAMES
    d546d5c3315b        higebobo/bind                                        "/usr/sbin/named -c …"   5 minutes ago       Up 5 minutes        53/udp, 0.0.0.0:53->53/tcp   bind
    ae23b3ae4e7f        registry.gitlab.com/higebobo/flask-rest-api-runner   "sh run.sh"              42 hours ago        Up 42 hours         0.0.0.0:15000->5000/tcp      docker_rest-api-test-server_1

run

    $ docker run --rm -it higebobo/bind:latest

## 所感

* Dockerhubのプライベートレポジトリは１つしか作れない
* Githubのorganizationのプライベートレポジトリは有料
    * Bitbucketならいいかもしれない(未確認)
* Github push から Dockerhub の image 作成までやや時間がかかる

## ToDO

* docker-compose
* log
* master/slave

## リンク

* [Docker 上で bind を動かす \| kurokobo\.com](http://blog.kurokobo.com/archives/2466) 
* [cosmicq/docker\-bind \- Docker Hub](https://hub.docker.com/r/cosmicq/docker-bind/)
* [【備忘録】dockerでbindを動かす \- TECHLOGICS](https://tech.hylogics.com/entry/using_bind_on_docker)
* [【備忘録】内部DNSのAlpine Linuxへの移行 \- TECHLOGICS](https://tech.hylogics.com/entry/bind_on_alpine_linux)
* [bind を Docker で動かす – philo式](http://philosy.com/blog/2019/05/27/bind-%E3%82%92-docker-%E3%81%A7%E5%8B%95%E3%81%8B%E3%81%99/)
* [＠IT：DNS Tips：ゾーンファイルの書き方について教えてください](https://www.atmarkit.co.jp/fnetwork/dnstips/031.html)

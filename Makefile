IMAGE=higebobo/bind
TAG=apline
NAME=toyoake-bind
DIR=bind

all: run

build:
	docker build -f ${DIR}/Dockerfile -t ${IMAGE}:${TAG} .

run: build
	docker run -d -p 53:53/udp -v `pwd`/${DIR}/conf:/etc/bind --restart always --name ${NAME} ${IMAGE}:${TAG}

stop:
	docker stop ${NAME}
	docker rm ${NAME}

clean: stop
	docker rmi ${IMAGE}:${TAG}

exec:
	docker exec -it ${NAME} "/bin/sh"

test:
	docker exec -it ${NAME} "/usr/bin/dig" "@127.0.0.1" "+short" "www.toyoake.local."

docker-login:
	docker login

docker-logout:
	docker logout

docker-push:
	docker push ${IMAGE}:${TAG}

add:
	git add .

commit: add
	git commit -m 'modified'

push: commit push-only

push-first:
	git push -u origin master

push-only:
	git push origin master
